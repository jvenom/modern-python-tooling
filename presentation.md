# Modern Python Tooling

---

# About Me
- Software Engineer @ USAA
- GitHub: [joshvernon](https://github.com/joshvernon)
- GitLab: [jvenom](https://gitlab.com/jvenom)
- Blog: [Venomous Software](https://jvenom.gitlab.io/venomous-software/)

---

# Agenda
- pipx
- poetry
- pre-commit
- black
- bandit and safety

---

# pipx
- Installs Python apps globally and puts them on your path
- Each app lives in its own environment
- You can upgrade, uninstall, and reinstall apps
- You can run commands inside the environments and manipulate them with `pip`
- Run **`pipx reinstall-all`** when you upgrade your Python installation
- **Bonus**: use **`pipx run`** to download a package to a temporary environment

---

# pipx
    !shell-session
    $ pipx list
    venvs are in /home/josh/.local/pipx/venvs
    apps are exposed on your $PATH at /home/josh/.local/bin
    package cookiecutter 1.7.2, Python 3.9.1
        - cookiecutter
    package landslide 1.1.8, Python 3.9.1
        - landslide
    package poetry 1.1.4, Python 3.9.1
        - poetry
    package pre-commit 2.9.3, Python 3.9.1
        - pre-commit
        - pre-commit-validate-config
        - pre-commit-validate-manifest

---

# pipx
Apps installed with pipx are globally accessible, even within a virtual environment.

    !shell-session
    $ pre-commit --version
    pre-commit 2.9.3
    $ which python; which pre-commit
    /usr/bin/python
    ~/.local/bin/pre-commit
    $ cd ~/codebase/python/microblog/
    $ poetry shell
    Spawning shell within /home/josh/.cache/pypoetry/virtualenvs/microblog-apC9teK7-py3.9
    . /home/josh/.cache/pypoetry/virtualenvs/microblog-apC9teK7-py3.9/bin/activate
    $ . /home/josh/.cache/pypoetry/virtualenvs/microblog-apC9teK7-py3.9/bin/activate
    $ pre-commit --version
    pre-commit 2.9.3
    $ which python; which pre-commit
    ~/.cache/pypoetry/virtualenvs/microblog-apC9teK7-py3.9/bin/python
    ~/.local/bin/pre-commit

---

# poetry - One tool to rule them all
- "Python packaging **_and_** dependency management made easy"
- Manages environments for you
- Allows you to ditch<sup>*</sup> pip, virtualenv, conda, and setuptools (at least in your day-to-day work)
- Supports configs for other tools (e.g. [black](https://black.readthedocs.io/en/stable/pyproject_toml.html))
- Publishing packages is as easy as `poetry build`, followed by `poetry publish`
- Follows PEPs [517](https://www.python.org/dev/peps/pep-0517/) and [518](https://www.python.org/dev/peps/pep-0518/)

---

# pyproject.toml

    [tool.poetry]
    name = "microblog"
    version = "0.1.0"
    description = "Example application from the Flask Mega-Tutorial"
    authors = ["Your Name <you@example.com>"]
    license = "MIT"

    [tool.poetry.dependencies]
    python = "^3.9"
    Flask = "^1.1.2"
    Flask-WTF = "^0.14.3"
    Flask-SQLAlchemy = "^2.4.4"
    Flask-Migrate = "^2.5.3"
    Flask-Login = "^0.5.0"
    email-validator = "^1.1.2"

    [tool.poetry.dev-dependencies]
    pylint = "^2.6.0"
    rope = "^0.18.0"
    python-dotenv = "^0.15.0"

    [tool.black]
    line-length = 88
    exclude = 'migrations'

    [build-system]
    requires = ["poetry-core>=1.0.0"]
    build-backend = "poetry.core.masonry.api"

---

# pre-commit
- Simple way to manage [git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) and reuse them across projects
- Lots of existing hooks available (look for a `.pre-commit-hooks.yaml` file in the tool's repo)
- Allows you to use a tool without having to install it directly
- Can also be run in CI pipelines

---

# .pre-commit-config.yaml

    !YAML
    repos:
    - repo: https://github.com/pre-commit/pre-commit-hooks
      rev: v3.4.0
      hooks:
      - id: check-yaml
      - id: end-of-file-fixer
      - id: mixed-line-ending
        args: [--fix=lf]
    - repo: https://github.com/psf/black
      rev: 20.8b1
      hooks:
      - id: black
    - repo: https://github.com/asottile/reorder_python_imports
      rev: v2.3.6
      hooks:
      - id: reorder-python-imports

---

# pre-commit in action

    !shell-session
    $ echo -e "\n\n\n\n\n\n\n" >> presentation.cfg 
    $ git add presentation.cfg
    $ git commit -m "Trigger end-of-file-fixer pre-commit hook"
    Check Yaml...........................................(no files to check)Skipped
    Fix End of Files.........................................................Failed
    - hook id: end-of-file-fixer
    - exit code: 1
    - files were modified by this hook

    Fixing presentation.cfg

    Mixed line ending........................................................Passed

---

# black
- "The uncompromising code formatter"
- Lots of Python projects are adopting it
- Hasn't really been as obtrusive as I thought it'd be
- The biggest thing I've noticed is that it changes single-quoted 'strings' to double-quoted "strings"
- Easy integration with poetry and pre-commit
- Can be run in a CI pipeline via `black --check`

---

# bandit and safety

## bandit
- Static security checker for your Python app code
- Maintained by [PyCQA](https://github.com/PyCQA) (Python Code Quality Authority), the same project that maintains pylint, pyflakes, and other tools

## safety
- Tool to check for security vulnerabilities in Python dependencies
- Maintained by [pyup.io](https://pyup.io/)
- The CLI package is free to install
- The free vulnerability database is updated once a month
- You have to pay if you want more frequent updates
- Needs a `requirements.txt` file

---

# example bandit run

    !shell-session
    $ bandit -r .
    [main]	INFO	profile include tests: None
    [main]	INFO	profile exclude tests: None
    [main]	INFO	cli include tests: None
    [main]	INFO	cli exclude tests: None
    [main]	INFO	running on Python 3.9.1
    Run started:2020-12-31 20:28:53.208301

    Test results:
    >> Issue: [B303:blacklist] Use of insecure MD2, MD4, MD5, or SHA1 hash function.
    Severity: Medium   Confidence: High
    Location: ./app/models.py:32
    More Info: https://bandit.readthedocs.io/en/latest/blacklists/blacklist_calls.html#b303-md5
    31	    def avatar(self, size):
    32	        digest = md5(self.email.lower().encode("utf-8")).hexdigest()
    33	        return f"https://www.gravatar.com/avatar/{digest}?d=identicon&s={size}"

    --------------------------------------------------

    Code scanned:
        Total lines of code: 310
        Total lines skipped (#nosec): 0

    Run metrics:
        Total issues (by severity):
            Undefined: 0.0
            Low: 0.0
            Medium: 1.0
            High: 0.0
        Total issues (by confidence):
            Undefined: 0.0
            Low: 0.0
            Medium: 0.0
            High: 1.0
    Files skipped (0):

---

# example safety run

    !shell-session
    $ safety check -r requirements.txt
    +==============================================================================+
    |                                                                              |
    |                               /$$$$$$            /$$                         |
    |                              /$$__  $$          | $$                         |
    |           /$$$$$$$  /$$$$$$ | $$  \__//$$$$$$  /$$$$$$   /$$   /$$           |
    |          /$$_____/ |____  $$| $$$$   /$$__  $$|_  $$_/  | $$  | $$           |
    |         |  $$$$$$   /$$$$$$$| $$_/  | $$$$$$$$  | $$    | $$  | $$           |
    |          \____  $$ /$$__  $$| $$    | $$_____/  | $$ /$$| $$  | $$           |
    |          /$$$$$$$/|  $$$$$$$| $$    |  $$$$$$$  |  $$$$/|  $$$$$$$           |
    |         |_______/  \_______/|__/     \_______/   \___/   \____  $$           |
    |                                                          /$$  | $$           |
    |                                                         |  $$$$$$/           |
    |  by pyup.io                                              \______/            |
    |                                                                              |
    +==============================================================================+
    | REPORT                                                                       |
    | checked 20 packages, using default DB                                        |
    +==============================================================================+
    | No known security vulnerabilities found.                                     |
    +==============================================================================+

---

# Conclusion
- Stand on the shoulders of giants
- Reduce your toil
- Always be thinking of new ways to do things
- Use [poetry](https://github.com/python-poetry/poetry#why)

## Resources
- [Source for these slides](https://gitlab.com/jvenom/modern-python-tooling)
- [sample project](https://gitlab.com/jvenom/flask-mega-tutorial-microblog) (microblog app from Miguel Grinberg's Flask Mega-Tutorial)

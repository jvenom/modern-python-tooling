# modern-python-tooling

Source for the ["Modern Python Tooling"](https://jvenom.gitlab.io/modern-python-tooling) presentation.

The slides are built with [landslide](https://github.com/adamzap/landslide) using the `leapmotion` theme.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## Editing the presentation content

The [presentation.md](presentation.md) Markdown file contains the presentation content. You can use any markdown you want in the slides, with the following caveats:
- The title slide must be a single-line `<h1>` and nothing else, e.g. `# My Title Slide`. This means you won't be able to put a subtitle or author info on the title slide.
- "Normal" markdown code blocks using back ticks don't work. Use indented code blocks instead, and add language highlighting with [pygments](https://pygments.org/)-style directives.
  ````
  # Don't do do this
  ```python
  def is_leap_year(year):
      return True if year % 4 == 0 else False
  ```
  ````

  ```
  # Do this instead - the blank line before the "!" directive is very important!

      !python
      def is_leap_year(year):
          return True if year % 4 == 0 else False
  ```

See the [landslide docs](https://github.com/adamzap/landslide#formatting) for other formatting rules.

## Building locally

Install the `landslide` Python package with pip or [pipx](https://pipxproject.github.io/pipx/). I chose to install it with pipx so that it's available globally on my development machine.

Run landslide:
```
landslide presentation.cfg
```

You should now have an `index.html` file in the same directory as your `presentation.md` file. Open `index.html` in a web browser to view the presentation.

## Deploying to GitLab Pages

On every push to `master`, GitLab CI will generate the `index.html` page and deploy it to GitLab Pages, along with the theme's assets. You shouldn't check `index.html` into source control (which is why it's in the `.gitignore`).
